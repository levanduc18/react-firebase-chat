Chat Application using React.JS + Firebase real-time Database + Firebase Gmail Authentication
=====================================

I build this app for learning purpose. you can check the live 💁‍♂️ [demo](https://cat-chat-withreact.herokuapp.com/) 



Quick Start:
------------
```bash
git clone
cd react-firebase-chat
✏️ *make sure you have to create a firebase config file on /src*
npm install
npm start
```

